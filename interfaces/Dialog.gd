extends Control

var dialog = ["Hola Mundo","Esto es un dialogo de Prueba","Esto es solo un super test"]
var dialogIndex = 0
var text_speed = 0.03
var finished = false

signal endText

func _ready():
	hideAll()

# warning-ignore:unused_argument
func _process(delta):
	$Indicator.visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		loadDialog()

func loadDialog():
	if dialogIndex < dialog.size():
		finished = false
		$Texto.bbcode_text = dialog[dialogIndex]
		$Texto.percent_visible = 0
		var animation_duration = text_speed * dialog[dialogIndex].length()
		$Texto/TextAnimation.interpolate_property(
			$Texto,"percent_visible",0,1,animation_duration, 
			Tween.TRANS_LINEAR,Tween.EASE_IN_OUT
		)
		$Texto/TextAnimation.start()
	else:
		hideAll()
		hide()
		emit_signal("endText")
	dialogIndex += 1

func startDialog(nDialog,character):
	showCharacter(character)
	dialog = nDialog
	dialogIndex = 0
	show()
	loadDialog() 


# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_TextAnimation_tween_completed(object, key):
	finished = true
	$Indicator.visible = finished

func hideAll():
	$Helper.hide()
	$Mele.hide()
	$Femele.hide()
	$NoBin.hide()
	$ARNp.hide()
	$Ribosoma.hide()
	$ARNm.hide()
	$Protein.hide()

func showPlayer():
	if(Global.noBin):
		$NoBin.show()
	elif(Global.isFemele):
		$Femele.show()
	else:
		$Mele.show()

func showCharacter(character):
	match character:
		"helper":
			$Name.text = "Voz Misteriosa"
			$Helper.show()
		"player":
			$Name.text = Global.playerName
			showPlayer()
		"arnp":
			$Name.text = "ARN Polimerasa"
			$ARNp.show()
		"ribosoma":
			$Name.text = "Julio"
			$Ribosoma.show()
		"arnm":
			$Name.text = "ARN Mensajero"
			$ARNm.show()
		"proteina":
			$Name.text = "Proteina"
			$Protein.show()
