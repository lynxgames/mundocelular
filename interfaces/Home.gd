extends Control

var femele = true
var noBin = false

# Called when the node enters the scene tree for the first time.
func _ready():
	hide()
	changePlayerModel()

func _on_PlayButton_pressed():
	Global.isFemele = femele
	Global.noBin = noBin
	Global.playerName = $LineEdit.text
	queue_free()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Despertar.tscn")

func _on_FemeleButton_pressed():
	femele = true
	noBin = false
	changePlayerModel()

func _on_MeleButton_pressed():
	femele = false
	noBin = false
	changePlayerModel()

func changePlayerModel():
	if (noBin):
		$PlayerNoBin.show()
		$PlayerMele.hide()
		$PlayerFemele.hide()
	elif (femele):
		$PlayerFemele.show()
		$PlayerMele.hide()
		$PlayerNoBin.hide()
	else:
		$PlayerMele.show()
		$PlayerFemele.hide()
		$PlayerNoBin.hide()


func _on_NoBinButton_pressed():
	femele = false
	noBin = true
	changePlayerModel()
