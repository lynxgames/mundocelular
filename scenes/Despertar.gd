extends Node2D

var bienvenida = [
	"Hey "+ Global.playerName +", despierta...",
	"Vamos no puedes seguir durmiendo, necesito tu ayuda..."
]
var resp1 = [
	"Eh...",
	"...",
	"¿Quién me esta hablando?",
	"Esta es alguna otra pesadilla rara..."
]
var resp2 = [
	"¿Qué? no es ninguna pesadilla, vamos arriba...",
	"...",
	"Levántate tenemos que irnos cuanto antes, no hay mucho tiempo...",
	"Hay una [color=red]célula[/color] que está necesitando ayuda para poder [color=red]sintetizar[/color] una [color=red]proteína[/color]. Podrías vivir una aventura sin igual..."
]
var resp3 = [
	"¿[color=red]Célula[/color]?",
	"...",
	"¿[color=red]Proteína[/color]?",
	"¿De qué me estás hablando? No entiendo nada..."
]
var resp4 = [
	"No hay tiempo de explicarte ahora...",
	"...",
	"Ven conmigo, te ayude a lo largo de tu aventura..."
]

# Called when the node enters the scene tree for the first time.
func _ready():
	show()
	$Trancicion.hide()
	$HelperButton.hide()
	inicioDeJuego()

func inicioDeJuego():
	$Dialog.startDialog(bienvenida, "helper")
	yield($Dialog,"endText")
	$Habitacion.show()
	$Dialog.startDialog(resp1, "player")
	yield($Dialog,"endText")
	$Dialog.startDialog(resp2, "helper")
	yield($Dialog,"endText")
	$Dialog.startDialog(resp3, "player")
	yield($Dialog,"endText")
	$Dialog.startDialog(resp4, "helper")
	yield($Dialog,"endText")
	$HelperButton.show()

func _on_HelperButton_pressed():
	$Trancicion.show()
	$Trancicion.play()
	yield($Trancicion,"finished")
	queue_free()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Celula.tscn")
