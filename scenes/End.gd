extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$Background.show()
	$CreadoPor.show()
	$Persojes.hide()
	$ARNp.hide()
	$ARNpIMG.hide()
	$ARNm.hide()
	$ARNmIMG.hide()
	$ProteinaIMG.hide()
	$Proteina.hide()
	$Player.hide()
	$Femele.hide()
	$Mele.hide()
	$NoBin.hide()
	$Cerrar.hide()
	$Julio.hide()
	$JulioIMG.hide()
	start()

func start():
	Global.await(3)
	yield(Global,"timeout")
	$CreadoPor.hide()
	$Persojes.show()
	Global.await(2)
	yield(Global,"timeout")
	$Persojes.hide()
	$ARNp.show()
	$ARNpIMG.show()
	Global.await(4)
	yield(Global,"timeout")
	$ARNp.hide()
	$ARNpIMG.hide()
	$Proteina.show()
	$ProteinaIMG.show()
	Global.await(4)
	yield(Global,"timeout")
	$Proteina.hide()
	$ProteinaIMG.hide()
	$Julio.show()
	$JulioIMG.show()
	Global.await(4)
	yield(Global,"timeout")
	$Julio.hide()
	$JulioIMG.hide()
	$Player.text = Global.playerName
	$Player.show()
	if (Global.noBin):
		$NoBin.show()
	elif (Global.isFemele):
		$Femele.show()
	else:
		$Mele.show()
	Global.await(4)
	yield(Global,"timeout")
	$Mele.hide()
	$Femele.hide()
	$NoBin.hide()
	$Player.hide()
	$Cerrar.show()


func _on_Cerrar_pressed():
	queue_free()
	get_tree().quit()
