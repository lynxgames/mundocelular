extends Node2D

var inADN = false
var inARNp = false
var ARNpFriend = false

var adnchat1 = [
	"Perfecto... Ya encontraste el [color=red]ADN[/color], aunque claro no nos sirve de mucho porque no sabemos cómo realizar la [color=red]transcripción[/color]...",
	"Deberías buscar a alguien aquí que pueda ayudarte a hacer la [color=red]transcripción[/color] que necesitamos para poder generar el [color=red]ARNm[/color].",
	"No debería estar muy lejos de aquí..."
]
var adnchat2 = [
	"Entiendo... Buscaré a alguien que pueda ayudarnos con eso..."
]
var adnchat1_2 = [
	"Perfecto, ya tienes el [color=red]ARNm[/color], aunque... No podemos volver al [color=red]Citosol[/color] así… Primero deberíamos sacar las partes que no sirven...",
	"Este proceso se conoce como [color=red]Splicing[/color] y se lleva a cabo aquí, en el [color=red]núcleo[/color]."
]
var arnchat1 = [
	"Hola, soy el [color=red]ARN Polimerasa[/color] (ARNp) yo trabajo acá, en el [color=red]núcleo[/color].",
	"Mi trabajo consiste en abrir las [color=red]hebras[/color] del [color=red]ADN[/color], rompiendo los [color=red]puentes hidrógenos[/color], para poder realizar la [color=red]transcripción[/color]."
]
var arnchat2 = [
	"¡Hey! Justo lo que estaba buscando..."
]
var arnchat3 = [
	"Ehh...",
	"...",
	"¿Puedo ayudarte en algo?"
]
var arnchat4 = [
	"Si, necesitamos tu ayuda, ven conmigo..."
]
var arnchat2_1 = [
	"Vamos, te sigo."
]
var arnm_chat_1 = [
	"¡Hola! gracias por eso...",
	"Soy el [color=red]ARN Mensajero[/color], ahora que contengo la información correcta podemos ir al [color=red]Ribosoma[/color] para comenzar con la producción de las [color=red]proteínas[/color].",
	"Vamos, acompáñame..."
]

func _ready():
	show()
	$BackToCitosol.hide()
	$Dialog.hide()
	$ARNPolimerasa.hide()
	$IngresoADN.hide()
	$ReplicaADN.hide()
	$Recorte.hide()
	$RecortePlayer.hide()
	$Out.hide()
	$Transcripcion.hide()

func _on_ADN_pressed():
	if (!inADN):
		inADN = true
		if (!ARNpFriend):
			$IngresoADN.show()
			$IngresoADN.play()
			yield($IngresoADN,"finished")
			$IngresoADN.hide()
			$Dialog.startDialog(adnchat1, "helper")
			yield($Dialog,"endText")
			$Dialog.startDialog(adnchat2, "player")
			yield($Dialog,"endText")
			$ARNPolimerasa.show()
			Global.await(2)
			yield(Global,"timeout")
		else:
			$ADN.hide()
			$Transcripcion.show()
			$Transcripcion.start()
			yield($Transcripcion,"endGame")
			$Transcripcion.hide()
			$ReplicaADN.show()
			$ReplicaADN.play()
			yield($ReplicaADN,"finished")
			$ReplicaADN.hide()
			$Dialog.startDialog(adnchat1_2,"helper")
			yield($Dialog,"endText")
			Global.celula_containARNm = true
			$Recorte.show()
			$ARNPolimerasa.hide()
		inADN = false

func _on_ARNPolimerasa_pressed():
	if(!inARNp):
		inARNp = true
		if (!ARNpFriend):
			$Dialog.startDialog(arnchat1, "arnp")
			yield($Dialog,"endText")
			$Dialog.startDialog(arnchat2, "player")
			yield($Dialog,"endText")
			$Dialog.startDialog(arnchat3, "arnp")
			yield($Dialog,"endText")
			$Dialog.startDialog(arnchat4, "player")
			yield($Dialog,"endText")
			ARNpFriend = true
		else:
			$Dialog.startDialog(arnchat2_1, "arnp")
			yield($Dialog,"endText")
		Global.await(2)
		yield(Global,"timeout")
		inARNp = false


func _on_BackToCitosol_pressed():
	$Out.show()
	$Out.play()
	yield($Out,"finished")
	$Out.hide()
	queue_free()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Celula.tscn")


func _on_Recorte_pressed():
	$Recorte.hide()
	$RecortePlayer.show()
	$RecortePlayer.play()
	yield($RecortePlayer,"finished")
	$RecortePlayer.hide()
	$Dialog.startDialog(arnm_chat_1, "arnm")
	yield($Dialog,"endText")
	$BackToCitosol.show()
