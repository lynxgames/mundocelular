extends Node2D

var inRibosoma = false

var chat1 = [
	"Llegamos al [color=red]Citosol[/color] o también conocido como [color=red]Citoplasma[/color].",
	"En este maravilloso lugar ocurren cosas magníficas, tantas que no nos daría el tiempo de explicar todas ahora...",
	"Bueno es hora de iniciar tu aventura, ve a buscar el [color=red]ADN[/color] lo necesitamos para obtener la secuencia necesaria para [color=red]sintetizar[/color] la [color=red]proteína[/color]...",
	"¡Oh! Lo olvidaba, antes de ir a buscar el [color=red]núcleo[/color] te recomiendo recorrer un poco el [color=red]Citosol[/color], Hay mucha información que podría ser muy importante..."
]
var ribosoma1 = [
	"Hola, soy Julio, un trabajador del [color=red]ribosoma[/color].",
	"En los ribosomas producimos las [color=red]proteínas[/color], ahora estamos esperando al [color=red]ARN Mensajero[/color] para comenzar la producción.",
	"El [color=red]ARN Mensajero[/color] contiene toda la información que necesitamos para poder producir las [color=red]proteínas[/color], pero parece que está un poco demorado..."
]
var arnm_chat_1 = [
	"Hola, ya estoy aquí gracias a " + Global.playerName + " que me dio una mano...",
	"Ahora podemos iniciar..."
]
var arnm_chat_2 = [
	"Hey " + Global.playerName + ", nos vemos de nuevo...",
	"Gracias por acompañar al [color=red]ARN Mensajero[/color] hasta aquí, ahora comenzaremos a producir la [color=red]Proteina[/color]."
]
var protein_chat = [
	"Hola, soy la [color=red]proteína[/color] que acaban de [color=red]sintetizar[/color], me gustaría quedarme a hablar pero tengo trabajo que hacer..."
]

func _ready():
	$Dialog.hide()
	$GoToNucleo.hide()
	$FinishGame.hide()
	if (Global.celula_isFirst):
		precentacion()
		Global.celula_isFirst = false
		$NucleoButton.show()
	else:
		$NucleoButton.hide()

func precentacion():
	Global.await(2)
	yield(Global,"timeout")
	$Dialog.startDialog(chat1, "helper")
	yield($Dialog,"endText")

func _on_NucleoButton_pressed():
	$GoToNucleo.show()
	$GoToNucleo.play()
	yield($GoToNucleo,"finished")
	queue_free()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Nucleo.tscn")

func _on_RobosomaButton_pressed():
	if (!inRibosoma):
		inRibosoma = true
		if (!Global.celula_containARNm):
			$Dialog.startDialog(ribosoma1, "ribosoma")
			yield($Dialog,"endText")
		else:
			$Dialog.startDialog(arnm_chat_1, "arnm")
			yield($Dialog,"endText")
			$Dialog.startDialog(arnm_chat_2, "ribosoma")
			yield($Dialog,"endText")
			$FinishGame.show()
			$FinishGame.play()
			yield($FinishGame,"finished")
			$FinishGame.hide()
			$Dialog.startDialog(protein_chat, "proteina")
			yield($Dialog,"endText")
			queue_free()
# warning-ignore:return_value_discarded
			get_tree().change_scene("res://scenes/End.tscn")
		Global.await(2)
		yield(Global,"timeout")
		inRibosoma = false
