extends Node2D

# Game Data
var basesNList = ["a","c","g","t","c"]
var baseActual

#Chat
var chat1 = [
	"Bien, vamos a dar una mano con la [color=red]Transcripción[/color], no es muy complejo pero debes prestar atención...",
	"Vamos a ir viendo [color=red]Bases Nitrogenadas[/color] y debemos ir seleccionando las [color=red]Bases Libres[/color] correspondientes.",
	"Si vemos una [color=blue]Adenina[/color] debemos elegir [color=red]Uracilo[/color].",
	"Si vemos una [color=purple]Citosina[/color] debemos elegir [color=green]Guanina[/color].",
	"Si vemos una [color=green]Guanina[/color] debemos elegir [color=purple]Citosina[/color].",
	"Si vemos una [color=red]Timina[/color] debemos elegir [color=blue]Adenina[/color]."
]

# Señales
signal endGame

func _ready():
	$Dialog.hide()
	hideAll()

func start():
	Global.await(1)
	yield(Global,"timeout")
	$Dialog.startDialog(chat1, "helper")
	yield($Dialog,"endText")
	nextBaseN()

func nextBaseN():
	if (basesNList.size() > 0):
		hideAll()
		baseActual = basesNList.pop_front()
		showBase(baseActual)
	else:
		Global.await(1)
		yield(Global,"timeout")
		emit_signal("endGame")

func hideAll():
	$AdeninaADN.hide()
	$AdeninaARN.hide()
	$GuaninaADN.hide()
	$GuaninaARN.hide()
	$UraciloARN.hide()
	$TiminaADN.hide()
	$CitosinaADN.hide()
	$CitosinaARN.hide()
	$Ok.hide()
	$Fail.hide()
	$LabelADN.hide()
	$LabelARN.hide()

func showBase(base):
	match base:
		"a":
			$AdeninaADN.show()
			$LabelADN.text = "Adenina"
			$LabelADN.show()
		"g":
			$GuaninaADN.show()
			$LabelADN.text = "Guanina"
			$LabelADN.show()
		"c":
			$CitosinaADN.show()
			$LabelADN.text = "Citosina"
			$LabelADN.show()
		"t":
			$TiminaADN.show()
			$LabelADN.text = "Timina"
			$LabelADN.show()

func _on_AdeninaBtn_pressed():
	$AdeninaARN.show()
	$LabelARN.text = "Adenina"
	$LabelARN.show()
	Global.await(1)
	yield(Global,"timeout")
	if (baseActual == "t"):
		$Ok.show()
		Global.playerPoints += 50
		Global.await(2)
		yield(Global,"timeout")
		$Ok.hide()
		nextBaseN()
	else:
		$Fail.show()
		Global.playerPoints -= 15
		Global.await(2)
		yield(Global,"timeout")
		$AdeninaARN.hide()
		$LabelARN.hide()
		$Fail.hide()


func _on_GuaninaBtn_pressed():
	$GuaninaARN.show()
	$LabelARN.text = "Guanina"
	$LabelARN.show()
	Global.await(1)
	yield(Global,"timeout")
	if (baseActual == "c"):
		$Ok.show()
		Global.playerPoints += 50
		Global.await(2)
		yield(Global,"timeout")
		$Ok.hide()
		nextBaseN()
	else:
		$Fail.show()
		Global.playerPoints -= 15
		Global.await(2)
		yield(Global,"timeout")
		$GuaninaARN.hide()
		$LabelARN.hide()
		$Fail.hide()


func _on_UraciloBtn_pressed():
	$UraciloARN.show()
	$LabelARN.text = "Uracilo"
	$LabelARN.show()
	Global.await(1)
	yield(Global,"timeout")
	if (baseActual == "a"):
		$Ok.show()
		Global.playerPoints += 50
		Global.await(2)
		yield(Global,"timeout")
		$Ok.hide()
		nextBaseN()
	else:
		$Fail.show()
		Global.playerPoints -= 15
		Global.await(2)
		yield(Global,"timeout")
		$UraciloARN.hide()
		$LabelARN.hide()
		$Fail.hide()

func _on_CitosinaBtn_pressed():
	$CitosinaARN.show()
	$LabelARN.text = "Citosina"
	$LabelARN.show()
	Global.await(1)
	yield(Global,"timeout")
	if (baseActual == "g"):
		$Ok.show()
		Global.playerPoints += 50
		Global.await(2)
		yield(Global,"timeout")
		$Ok.hide()
		nextBaseN()
	else:
		$Fail.show()
		Global.playerPoints -= 15
		Global.await(2)
		yield(Global,"timeout")
		$CitosinaARN.hide()
		$LabelARN.hide()
		$Fail.hide()
