extends Node

var playerName = ""
var isFemele = true
var noBin = false
var playerPoints = 0

# Global To Celulas
var celula_isFirst = true
var celula_containARNm = false

signal timeout

func _ready():
	pass

# warning-ignore:unused_argument
func _process(delta):
	if (!GlobalScene.getAudio().is_playing()):
		GlobalScene.getAudio().play()

func getAudio():
	return $AudioStreamPlayer

func await(time):
	var t = Timer.new()
	t.set_wait_time(time)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	emit_signal("timeout")
	t.queue_free()
